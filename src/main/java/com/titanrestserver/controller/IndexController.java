package com.titanrestserver.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.titanrestserver.Global;
import com.titanrestserver.json.RestResult;

@Controller
public class IndexController {
	private static Logger logger = Logger.getLogger(IndexController.class);

	@RequestMapping(value = "/index.htm", method = RequestMethod.GET, produces = "application/json")
	public String index(ModelMap model) {
		//		checkTableExist();
		return "index";
	}

	@RequestMapping(value = "/init.htm", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public RestResult init(int titanServerPort) {
		logger.info("ini titanServerPort=" + titanServerPort);
		Global.titanServerPort = titanServerPort;
		RestResult result = new RestResult();
		result.setName("result");
		result.getValues().put("testMessage", "init success");
		return result;
	}

	@RequestMapping(value = "/test.htm", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public RestResult test(String testMessage) {
		logger.info("test, testMessage=" + testMessage);
		RestResult result = new RestResult();
		result.setName("result");
		result.getValues().put("testMessage", "Got your test message=" + testMessage);
		result.getValues().put("testMessage", "Titan server port=" + Global.titanServerPort);
		return result;
	}

	@RequestMapping(value = "/login.htm", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public RestResult login(String username, String password) {
		logger.info("login, username=" + username);
		RestResult result = new RestResult();
		result.setName("result");
		if (username != null && password != null && username.equals("admin") && password.equals("1234")) {
			result.getValues().put("loginResult", "ok");
		} else {
			result.getValues().put("loginResult", "fail");
		}

		return result;
	}
}
