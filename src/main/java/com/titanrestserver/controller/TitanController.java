package com.titanrestserver.controller;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Random;

import javax.net.SocketFactory;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.titancommon.Command;
import com.titancommon.ReturnCommand;
import com.titanrestserver.Global;
import com.titanrestserver.json.RestResult;

@Controller
@RequestMapping("/titan")
public class TitanController {
	private static Logger logger = Logger.getLogger(TitanController.class);

	@RequestMapping(value = "/sendCommand.htm", produces = "application/json")
	@ResponseBody
	public RestResult sendCommand(@RequestParam LinkedHashMap<String, String> parameters) {
		int rand = new Random().nextInt(1000000);
		String titanCommand = parameters.get("titanCommand");
		parameters.remove("titanCommand");
		logger.info("sendCommand.htm, [" + rand + "] titanCommand=" + titanCommand);
		RestResult result = new RestResult();
		result.setName("result");
		if (titanCommand == null) {
			return result;
		}
		Socket socket = null;
		try {
			SocketAddress remoteaddr = new InetSocketAddress("localhost", Global.titanServerPort);
			socket = SocketFactory.getDefault().createSocket();
			socket.connect(remoteaddr, 3000);

			ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
			ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
			Command command = new Command();
			command.command = titanCommand;
			command.parameters.add(parameters);
			command.date = new Date();
			out.writeObject(command);
			out.flush();
			ReturnCommand r = (ReturnCommand) in.readObject();
			result.getValues().put("result", r);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("sendCommand.htm, [" + rand + "] finished, titanCommand=" + titanCommand);
		return result;
	}
}
