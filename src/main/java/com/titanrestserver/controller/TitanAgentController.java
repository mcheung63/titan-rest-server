package com.titanrestserver.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.poi.util.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.titanrestserver.Global;
import com.titanrestserver.json.RestResult;

@Controller
@RequestMapping("/titanAgent")
public class TitanAgentController {
	private static Logger logger = Logger.getLogger(TitanAgentController.class);
	private static String username;
	private static String password;
	private static String db;
	private static String host;
	private static String url;

	@RequestMapping(value = "/vmStat.htm", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public RestResult vmStat(String version, String instanceId, String category, String message, MultipartFile image) {
		//		logger.info("vmStat, instanceId=" + instanceId);
		RestResult result = new RestResult();
		result.setName("result");
		try {
			if (url == null) {
				Properties prop = new Properties();
				FileInputStream inputStream = new FileInputStream(new File(getClass().getResource("/log4j.properties").toURI()));
				prop.load(inputStream);

				username = prop.getProperty("log4j.appender.mysql.user");
				password = prop.getProperty("log4j.appender.mysql.password");
				db = prop.getProperty("log4jDb");
				host = prop.getProperty("log4jHost");
				Class.forName("com.mysql.jdbc.Driver");
				url = "jdbc:mysql://" + host + "/" + db;
				inputStream.close();
			}

			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url, username, password);
			String sql = "insert into restserver_vmStat set date=now(), version=?, instanceId=?, category=?, message=?";
			PreparedStatement pmt = conn.prepareStatement(sql);
			pmt.setString(1, version);
			pmt.setString(2, instanceId);
			pmt.setString(3, category);
			pmt.setString(4, message);
			pmt.execute();
			conn.close();

			if (image != null) {
				File dir = new File("vmImages");
				if (!dir.exists()) {
					dir.mkdirs();
				}
				//				System.out.println(dir.getAbsolutePath() + File.separator + instanceId + sdf.format(new Date()) + ".jpg");
				IOUtils.copy(image.getInputStream(), new FileOutputStream(new File(dir.getAbsolutePath() + File.separator + instanceId + Global.sdf.format(new Date()) + ".jpg")));
			}

			result.getValues().put("result", "ok");
		} catch (Exception ex) {
			ex.printStackTrace();
			result.getValues().put("result", ex.getMessage());
		}

		return result;
	}
}
