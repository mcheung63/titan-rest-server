package com.titanrestserver.json;

import java.util.Hashtable;

public class RestResult {
	String name;
	Hashtable<String, Object> values = new Hashtable<String, Object>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Hashtable<String, Object> getValues() {
		return values;
	}

	public void setValues(Hashtable<String, Object> values) {
		this.values = values;
	}

}
