package com.titanrestserver;

import java.text.SimpleDateFormat;

public class Global {
	public static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
	public static int titanServerPort = -1;
}
