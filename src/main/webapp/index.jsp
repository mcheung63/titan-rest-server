<%@ page contentType="text/html; charset=UTF-8" language="java"
	errorPage=""%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Titan Rest Server</title>
	<style type="text/css">
	</style>
	<link rel="stylesheet" type="text/css" href="theme/<fmt:bundle basename="main"><fmt:message key="theme" /></fmt:bundle>/en/main.css" />
	<link rel="stylesheet" type="text/css" href="theme/<fmt:bundle basename="main"><fmt:message key="theme" /></fmt:bundle>/en/SexyButtons/sexybuttons.css" />
	<script type='text/javascript' src='jquery-1.8.3.min.js'></script>
</head>

<body>
	Titan Rest Server
</body>
</html>
